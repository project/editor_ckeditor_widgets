(function ($, Drupal) {

  'use strict';

  /**
   * Provides the summary for the "lite" plugin settings vertical tab.
   */
  Drupal.behaviors.ckeditorLITESettingsSummary = {
    attach: function attach() {
      $('#edit-editor-settings-plugins-lite').drupalSetSummary(function (context) {
        var root = 'input[name="editor[settings][plugins][lite]';
        var $is_tracking = $(root + '[is_tracking]"]');

        if (!$is_tracking.is(':checked')) {
          return Drupal.t('Change tracking starts disabled');
        }
        else {
          return Drupal.t('Change tracking starts enabled');
        }
      });
    }
  };

})(jQuery, Drupal);
