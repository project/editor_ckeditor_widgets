<?php

/**
 * @file
 * Editor CKEditor integration for the editor_ckeditor_widgets module.
 */

/**
 * Implements hook_editor_ckeditor_plugins().
 */
function editor_ckeditor_widgets_editor_ckeditor_plugins() {
  $plugins = array();
  $path = drupal_get_path('module', 'editor_ckeditor_widgets');

  if (($library = libraries_detect('leaflet')) && !empty($library['installed'])) {
    $plugins['leaflet'] = array(
      'label' => t('Leaflet'),
      'file' => $path . '/plugins/editor_ckeditor_widgets.leaflet.inc',
      'plugin' => $library['library path'] . '/plugin.js',
      'buttons' => array(
        'leaflet' => array(
          'label' => t('Leaflet'),
          'required_html' => array(
            array(
            'tags' => array('div'),
            'attributes' => array('id', 'data-lat', 'data-lon', 'data-width', 'data-height', 'data-zoom', 'data-popup-text', 'data-tile', 'data-minimap', 'data-alignment', 'data-responsive'),
            'classes' => array('leaflet_div', 'align-left', 'align-right', 'align-center', 'responsive-map'),
            ),
            array(
              'tags' => array('iframe'),
              'attributes' => array('id', 'height', 'width', 'data-lat', 'data-lon', 'data-width', 'data-height', 'data-zoom', 'data-popup-text', 'data-tile', 'data-minimap', 'data-alignment', 'data-responsive', 'src', 'data-cke-saved-src'),
              'classes' => array('leaflet_iframe', 'responsive-map-iframe'),
            ),
          ),
          'image' => $library['library path'] . '/icons/leaflet.png',
        ),
      ),
      'supports_configuration' => TRUE,
      'config callback' => 'editor_ckeditor_widgets_leaflet_config',
      'settings form' => 'editor_ckeditor_widgets_leaflet_settings_form',
    );
  }

  if (($library = libraries_detect('abbr')) && !empty($library['installed'])) {
    $plugins['abbr'] = array(
      'label' => t('Abbreviation'),
      'plugin' => $library['library path'] . '/plugin.js',
      'buttons' => array(
        'Abbr' => array(
          'label' => t('Abbreviation'),
          'required_html' => array(array(
            'tags' => array('abbr'),
            'attributes' => array('title', 'id'),
          )),
          'image' => $library['library path'] . '/icons/abbr.png',
        ),
      ),
    );
  }

  if (($library = libraries_detect('placeholder')) && !empty($library['installed'])) {
    $plugins['placeholder'] = array(
      'label' => t('Placeholder'),
      'plugin' => $library['library path'] . '/plugin.js',
      'buttons' => array(
        'CreatePlaceholder' => array(
          'label' => t('Placeholder'),
          'required_html' => array(array(
            'tags' => array('span'),
            'classes' => array('cke_placeholder'),
          )),
          'image' => $library['library path'] . '/icons/placeholder.png',
        ),
      ),
    );
  }

  if (($library = libraries_detect('timestamp')) && !empty($library['installed'])) {
    $plugins['timestamp'] = array(
      'label' => t('Timestamp'),
      'plugin' => $library['library path'] . '/plugin.js',
      'buttons' => array(
        'Timestamp' => array(
          'label' => t('Timestamp'),
          'required_html' => array(array(
            'tags' => array('em'),
          )),
          'image' => $library['library path'] . '/icons/timestamp.png',
        ),
      ),
    );
  }

  return $plugins;
}
