<?php

/**
 * @file
 * Defines the "leaflet" plugin.
 */

/**
 * Config callback for the 'leaflet' plugin.
 *
 * @param object $format
 *  A text format object.
 *
 * @return array
 *   An associative array of configuration settings.
 */
function editor_ckeditor_widgets_leaflet_config($format) {
  $config = array(
    'leaflet_maps_google_api_key' => 'AIzaSyA9ySM6msnGm0qQB1L1cLTMBdKEUKPySmQ'
  );

  $settings = $format->editor_settings;

  if (!isset($settings['plugins']['leaflet']['leaflet_maps_google_api_key'])) {
    return $config;
  }

  $leaflet_maps_google_api_key = $settings['plugins']['leaflet']['leaflet_maps_google_api_key'];
  $config['leaflet_maps_google_api_key'] = $leaflet_maps_google_api_key;

  return $config;
}

/**
 * Settings form for the "leaflet" CKEditor plugin.
 */
function editor_ckeditor_widgets_leaflet_settings_form($form, &$form_state, $format) {
  // Defaults.
  $config = array('leaflet_maps_google_api_key' => 'AIzaSyA9ySM6msnGm0qQB1L1cLTMBdKEUKPySmQ');
  $settings = $format->editor_settings;
  if (isset($settings['plugins']['leaflet'])) {
    $config = $settings['plugins']['leaflet'];
  }

  $form['leaflet_maps_google_api_key'] = array(
    '#title' => t('Google API key'),
    '#type' => 'textfield',
    '#default_value' => $config['leaflet_maps_google_api_key'],
    '#description' => t('Enter your <a href="@google_maps_api_key_url">Google Maps JavaScript API key</a>. <em>Google Maps JavaScript API</em> must be enabled in your <em>Google API Console</em> account/dashboard in order for the key to be active.', array('@google_maps_api_key_url' => 'https://developers.google.com/maps/documentation/javascript/get-api-key')),
    '#attached' => array(
      'library' => array(array('editor_ckeditor_widgets', 'editor_ckeditor_widgets.leaflet.admin')),
    ),
  );

  return $form;
}
