CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Technical details

INTRODUCTION
------------

Current Maintainers:

 * Devin Carlson <http://drupal.org/user/290182>

Editor CKEditor Widgets integrates with CKEditor to provide common embeddable
widgets.

REQUIREMENTS
------------

Editor CKEditor Widgets has two dependencies.

Contributed modules
 * Editor CKEditor - A submodule of the Editor module.
 * Libraries

INSTALLATION
------------

* Install Editor CKEditor Widgets via the standard Drupal installation process:
  'http://drupal.org/node/895232'.
* Enable any of the widgets by dragging their respective buttons into the active
  toolbar configuration for the desired text formats from the Text Formats
  configuration page: '/admin/config/content/formats'.

By default, Editor CKEditor Widgets does not include any widgets. Widgets must
be downloaded from their respective vendors.

AVAILABLE WIDGETS
-----------------
Leaflet - Embed mobile-friendly interactive maps from the open-source JavaScript library.
https://ckeditor.com/cke4/addon/leaflet

Placeholder - Create and edit placeholders (non-editable text fragments).
https://ckeditor.com/cke4/addon/placeholder

Abbreviation - Lets users insert abbreviations.
https://github.com/ckeditor/ckeditor4-docs-samples - Use 'abbr' folder located at 'tutorial-abbr-acf/abbr'

Timestamp - Inserts current date and time.
https://github.com/ckeditor/ckeditor4-docs-samples - Use 'timestamp' folder located at 'tutorial-timestamp/timestamp'
